package service

import (
	"context"
	"errors"

	"gitlab.com/a.bitiev/auth/internal/entity"
)

var (
	ErrUserNotFound      = errors.New("user not found")
	ErrUserAlreadyExists = errors.New("user already exists")
)

type (
	RegisterInput struct {
		Login    string
		Email    string
		Password string
		Phone    string
	}

	LoginInput struct {
		Login    string
		Password string
	}
)

type (
	UserStorage interface {
		CreateUser(context.Context, *entity.User) (int, error)
		GetUser(context.Context, string, string) (*entity.User, error)
	}

	PasswordHasher interface {
		Hash(string) (string, error)
	}
)
