package service

import (
	"context"
	"fmt"

	"gitlab.com/a.bitiev/auth/internal/entity"
)

type UserService struct {
	storage UserStorage
	hasher  PasswordHasher
}

func NewUserService(storage UserStorage, hasher PasswordHasher) *UserService {
	return &UserService{
		storage: storage,
		hasher:  hasher,
	}
}

func (s *UserService) Register(ctx context.Context, user *entity.User) (int, error) {
	passwordHash, err := s.hasher.Hash(user.Password)
	if err != nil {
		return 0, fmt.Errorf("failed to hash password: %w", err)
	}

	user.Password = passwordHash

	id, err := s.storage.CreateUser(ctx, user)
	if err != nil {
		return 0, fmt.Errorf("failed to create user: %w", err)
	}

	return id, nil
}

func (s *UserService) Login(ctx context.Context, login, password string) (*entity.User, error) {
	passwordHash, err := s.hasher.Hash(password)
	if err != nil {
		return nil, fmt.Errorf("failed to hash password: %w", err)
	}

	user, err := s.storage.GetUser(ctx, login, passwordHash)
	if err != nil {
		return nil, fmt.Errorf("failed to get user: %w", err)
	}

	return user, nil
}
