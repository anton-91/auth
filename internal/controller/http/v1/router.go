package v1

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

func InitRoutes(r chi.Router, service UserService, logger *logrus.Logger) {
	h := NewHandler(service, logger)

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.NotFound(notFoundHandler)
	r.MethodNotAllowed(methodNotAllowedHandler)

	r.Post("/login", h.Login)
	r.Post("/register", h.Register)
}

func methodNotAllowedHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusMethodNotAllowed)
	render.Render(w, r, ErrMethodNotAllowed)
}

func notFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	render.Render(w, r, ErrNotFound)
}
