package database

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
)

const (
	defaultMaxConns     = 1
	defaultConnAttempts = 5
	defaultConnTimeout  = time.Second
)

type PostgresDB struct {
	Pool   *pgxpool.Pool
	logger *logrus.Logger

	maxConns     int
	connAttempts int
	connTimeout  time.Duration
}

func NewPostgresDB(connString string, logger *logrus.Logger, options ...Option) (*PostgresDB, error) {
	postgres := &PostgresDB{
		logger:       logger,
		maxConns:     defaultMaxConns,
		connAttempts: defaultConnAttempts,
		connTimeout:  defaultConnTimeout,
	}

	for _, applyOption := range options {
		applyOption(postgres)
	}

	poolConfig, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	poolConfig.MaxConns = int32(postgres.maxConns)

	for postgres.connAttempts > 0 {
		postgres.logger.Infof("connecting to database, attempts left: %d...", postgres.connAttempts)
		postgres.Pool, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
		if err == nil {
			break
		}
		time.Sleep(postgres.connTimeout)
		postgres.connAttempts--
	}
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %w", err)
	}

	postgres.logger.Infof("connected to database")

	return postgres, nil
}

func (p *PostgresDB) Close() {
	p.logger.Info("closing connections pool...")
	if p.Pool != nil {
		p.Pool.Close()
	}
}
