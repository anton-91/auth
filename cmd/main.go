package main

import (
	"log"

	"gitlab.com/a.bitiev/auth/config"
	"gitlab.com/a.bitiev/auth/internal/app"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatalf("failed to init config: %v", err)
	}

	app.Run(cfg)
}
